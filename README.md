## Medical Examinations

### Uruchomienie aplikacji
Aplikację można uruchomić w standardowy sposób, czyli instalując LAMP (PHP 8) u uruchamiając komende `php artisan serve`, a następnie `php artisan medical:examinations`

Zalecam użyć Laravel Sail (docker) do odpalenia aplikacji:
- odpalenie dockera i instalacja zależności:
```
docker run --rm \
    -u "$(id -u):$(id -g)" \
    -v $(pwd):/var/www/html \
    -w /var/www/html \
    laravelsail/php80-composer:latest \
    composer install --ignore-platform-reqs
```
- Ewentualna konfiguracja bazy danych
- Odpalenie komendy:

```./vendor/bin/sail artisan medical:examinations```

- komenda ta: czyści cache, seeduje bazę danych i na końcu tworzy użytkownika, podając w konsoli dane do logowania po API token do zarządzania badaniami itd.

### Testy
Aby uruchomić testy wystarczy wpisać komendę:

``` ./vendor/bin/sail test ```

### Dokumentacja API
Dokumentacja API jest dostępna pod tym linkiem:
https://documenter.getpostman.com/view/17399088/U16gR8Bi

Opisany jest w niej sposób autoryzacji (Laravel Sanctum) oraz zastosowanie filtrów w poszczególnych endpointach.

### Użyte wzorce projektowe*
- **Pipeline** - filtrowanie badań po kategorii oraz prosta wyszukiwarka,
- **Decorator** - cachowanie danych w celu optymalizacji połączenia z bazą danych,
- **Repositories** - użyłem ich tylko dlatego, że zaimplementowałem dekorator, poza tym przydaje się do bardziej skomplikowanych zapytań, które się powtarzają. Inaczej nie widzę sensu implementacji tego wzorca,
- **Services** - żeby odciążyć kontrolery z logiki i sprawić by kod był czysty,
- Upchałbym jeszcze **Buildera**, bardzo go lubię ale nie było na niego miejsca w tym malutkim projekcie. 

*_(nie licząc tych we frameworku)_
