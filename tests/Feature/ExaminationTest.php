<?php

namespace Tests\Feature;

use App\Models\Category;
use App\Models\Examination;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Laravel\Sanctum\Sanctum;
use Tests\TestCase;

class ExaminationTest extends TestCase
{
    use RefreshDatabase;

    public function test_index(): void
    {
        $this
            ->getJson(route('api.examinations.index'))
            ->assertStatus(200);
    }

    public function test_show(): void
    {
        $examination = Examination::factory()->create();

        $this
            ->getJson(route('api.examinations.show', $examination))
            ->assertStatus(200);
    }

    public function test_store(): void
    {
        $this->actingAsSanctumUser();

        $this
            ->postJson(
                route('api.admin.examinations.store'),
                [
                    'name' => 'test examination',
                    'icd_10_code' => 'G44.311',
                ]
            )
            ->assertStatus(201);
    }

    public function test_update(): void
    {
        $this->actingAsSanctumUser();

        $examination = Examination::factory()->create([
            'name' => 'Test examination'
        ]);

        $response = $this->putJson(
            route('api.admin.examinations.update', $examination),
            ['name' => 'Edited examination']
        );


        $response->assertStatus(200);
        $this->assertSame($examination->refresh()->name, 'Edited examination');
    }

    public function test_destroy(): void
    {
        $this->actingAsSanctumUser();

        $examination = Examination::factory()->create();

        $this
            ->deleteJson(route('api.admin.examinations.destroy', $examination))
            ->assertStatus(200);
    }

    public function test_sync_categories(): void
    {
        $this->actingAsSanctumUser();

        $examination = Examination::factory()->create();
        $categories = Category::factory()->count(10)->create();

        $this
            ->postJson(
                route('api.admin.examinations.sync.categories', $examination),
                ['categories' => $categories->pluck('id')->toArray()]
            )
            ->assertStatus(200);
    }

    private function actingAsSanctumUser()
    {
        Sanctum::actingAs(
            User::factory()->create(),
            ['*']
        );
    }
}
