<?php

namespace Tests\Feature;

use App\Models\Category;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;
use Laravel\Sanctum\Sanctum;

class CategoryTest extends TestCase
{
    use RefreshDatabase;

    public function test_index(): void
    {
        $this
            ->getJson(route('api.categories.index'))
            ->assertStatus(200);
    }

    public function test_show(): void
    {
        $category = Category::factory()->create();

        $this
            ->getJson(route('api.categories.show', $category))
            ->assertStatus(200);
    }

    public function test_store(): void
    {
        $this->actingAsSanctumUser();

        $this
            ->postJson(
                route('api.admin.categories.store'),
                ['name' => 'test category']
            )
            ->assertStatus(201);
    }

    public function test_update(): void
    {
        $this->actingAsSanctumUser();

        $category = Category::factory()->create([
            'name' => 'Test category'
        ]);

        $response = $this->putJson(
            route('api.admin.categories.update', $category),
            ['name' => 'Edited category']
        );


        $response->assertStatus(200);
        $this->assertSame($category->refresh()->name, 'Edited category');
    }

    public function test_destroy(): void
    {
        $this->actingAsSanctumUser();

        $category = Category::factory()->create();

        $this
            ->deleteJson(route('api.admin.categories.destroy', $category))
            ->assertStatus(200);
    }

    private function actingAsSanctumUser()
    {
        Sanctum::actingAs(
            User::factory()->create(),
            ['*']
        );
    }
}
