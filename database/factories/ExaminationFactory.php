<?php

namespace Database\Factories;

use App\Models\Examination;
use Illuminate\Database\Eloquent\Factories\Factory;

class ExaminationFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Examination::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition(): array
    {
        return [
            'name' => $this->faker->sentence(),
            'icd_10_code' => $this->faker->regexify('[A-TV-Z][0-9][0-9AB]\.?[0-9A-TV-Z]{0,4}'),
            'short_description' => $this->faker->text(),
            'preparation_instructions' => $this->faker->text(),
        ];
    }
}
