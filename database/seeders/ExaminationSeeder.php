<?php

namespace Database\Seeders;

use App\Models\Category;
use App\Models\Examination;
use Illuminate\Database\Seeder;

class ExaminationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $categories = collect([
            [
                'name' => 'Badania podstawowe i biochemiczne',
                'examinations' => [
                    [
                        'name' => 'ALT',
                        'icd_10_code' => 'I17',
                        'short_description' => 'ALT. Oznaczenie  aktywności enzymu wątrobowego:  aminotransferazy alaninowej  (ALT), umożliwia rozpoznawanie, różnicowanie i ocenę ciężkości chorób wątroby. Stosowane jest w diagnostyce ostrych i przewlekłych stanów zapalnych wątroby. Najistotniejszą przyczyną wzrostu poziomu ALT we krwi są ostre choroby zapalne wątroby, natomiast najwyższy wzrost obserwowany jest w przypadku toksycznego lub niedokrwiennego uszkodzenia wątroby. ',
                        'preparation_instructions' => 'Krew pobierać na czczo (ze względu na ALT) między godz. 7.00-10.00. Zaleca się, by ostatni posiłek poprzedniego dnia był spożyty nie później niż o godz.18.00.',
                    ],
                    [
                        'name' => 'Cholesterol całkowity',
                        'icd_10_code' => 'I99',
                        'short_description' => 'Cholesterol całkowity.  Pomiar stężenia cholesterolu całkowitego (CHOL) jest przesiewowym badaniem cholesterolu, pozwalającym na wstępną ocenę ryzyka  rozwoju miażdżycy i chorób układu sercowo-naczyniowego, takich jak choroba wieńcowa i zawał serca, udar mózgu i miażdżyca tętnic kończyn dolnych itd. Cholesterol całkowity obejmuje trzy frakcje: lipoproteinę niskiej gęstości, LDL; lipoproteinę wysokiej gęstości, HDL (ang. high-density lipoprotein)  oraz  łącznie,  lipoproteinę bardzo małej gęstości,  VLDL i lipoproteinę o pośredniej gęstości, IDL.',
                        'preparation_instructions' => 'Badanie wykonywane na czczo. Zaleca się, aby ostatni posiłek poprzedniego dnia był spożyty nie później niż o godz.18.00. Krew do badania należy pobierać w godz. 7.00-10.00. Zaleca się stosowanie stałej diety w okresie kilku dni poprzedzających badanie.',
                    ],
                    [
                        'name' => 'Cholesterol LDL met. bezpośrednią',
                        'icd_10_code' => 'K03',
                        'short_description' => 'Pomiar stężenia cholesterolu frakcji LDL, przydatny w diagnostyce dyslipidemii oraz w ocenie ryzyka miażdżycy i chorób sercowo-naczyniowych.',
                        'preparation_instructions' => 'Badanie wykonywane na czczo. Zaleca się, aby ostatni posiłek poprzedniego dnia był spożyty nie później niż o godz.18.00. Krew do badania należy pobierać w godz. 7.00-10.00. Zaleca się stosowanie stałej diety w okresie kilku dni poprzedzających badanie.',
                    ],
                    [
                        'name' => 'Lipidogram (CHOL, HDL, nie-HDL, LDL, TG)',
                        'icd_10_code' => 'M71',
                        'short_description' => 'Lipidogram - ilościowa ocena frakcji cholesterolu i trójglicerydów, przydatna w diagnostyce dyslipidemii oraz w ocenie ryzyka miażdżycy i chorób sercowo-naczyniowych.',
                        'preparation_instructions' => 'Badanie wykonywane na czczo. Zaleca się, aby ostatni posiłek poprzedniego dnia był spożyty nie później niż o godz.18.00. Krew do badania należy pobierać w godz. 7.00-10.00. Zaleca się stosowanie stałej diety w okresie kilku dni poprzedzających badanie.',
                    ],

                ],],
            [
                'name' => 'Hematologia',
                'examinations' => [
                    [
                        'name' => 'Bilirubina wolna (pośrednia)',
                        'icd_10_code' => 'I91',
                        'short_description' => 'Bilirubina wolna. Pomiar stężenia bilirubiny wolnej (niesprzężonej, pośredniej)  w krwi, przydatny  w diagnostyce i różnicowaniu żółtaczek.',
                        'preparation_instructions' => 'Krew pobierać na czczo  między godz. 7.00-10.00. Zaleca się, by ostatni posiłek poprzedniego dnia był spożyty nie później niż o godz.18.00.',
                    ],
                    [
                        'name' => 'Łańcuchy lekkie lambda',
                        'icd_10_code' => 'M85',
                        'short_description' => 'Łańcuchy lekkie lambda w krwi. Test stosowany w diagnostyce gammapatii i chorób limfoproliferacyjnych.',
                        'preparation_instructions' => null,
                    ],
                ],
            ],
            [
                'name' => 'Choroby przewodu pokarmowego',
                'examinations' => [
                    [
                        'name' => 'CA 19-9',
                        'icd_10_code' => 'I45',
                        'short_description' => ' CA 19-9.  Oznaczany w surowicy marker raka trzustki, dróg wątrobowo-żółciowych oraz jelita  grubego i odbytnicy, przydatny w diagnostyce i postępowaniu z chorymi.',
                        'preparation_instructions' => 'Brak szczególnych wskazań.',
                    ],
                    [
                        'name' => 'Helicobacter pylori IgM',
                        'icd_10_code' => 'U13',
                        'short_description' => 'Helicobacter pylori IgM. Diagnostyka serologiczna zakażenia Helicobacter pylori. Oznaczenie poziomu przeciwciał IgM specyficznych dla H. pylori w surowicy krwi żylnej, przydatne w diagnostyce pierwotnego zakażenia H. pylorii.',
                        'preparation_instructions' => null,
                    ],
                ],
            ]
        ]);

        $categories->each(function ($category) {
            $categoryModel = Category::firstOrCreate(
                ['name' => data_get($category, 'name')],
            );

            $examinations = collect(data_get($category, 'examinations'))->map(function ($examination) {
                return Examination::firstOrCreate(
                    [
                        'icd_10_code' => data_get($examination, 'icd_10_code')
                    ],
                    [
                        'name' => data_get($examination, 'name'),
                        'short_description' => data_get($examination, 'short_description'),
                        'preparation_instructions' => data_get($examination, 'preparation_instructions'),
                    ]
                );
            });

            $categoryModel->examinations()->sync(
                $examinations->pluck('id')->toArray()
            );
        });
    }
}
