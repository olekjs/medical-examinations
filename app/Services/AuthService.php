<?php

namespace App\Services;

use App\Contracts\Repositories\UserRepositoryInterface;
use App\Models\User;
use Laravel\Sanctum\NewAccessToken;
use Illuminate\Contracts\Hashing\Hasher;

class AuthService
{
    /**
     * @var UserRepositoryInterface
     */
    private UserRepositoryInterface $userRepository;

    /**
     * @var Hasher
     */
    private Hasher $hashService;

    /**
     * AuthService constructor.
     * @param UserRepositoryInterface $userRepository
     * @param Hasher $hashService
     */
    public function __construct(
        UserRepositoryInterface $userRepository,
        Hasher $hashService
    )
    {
        $this->userRepository = $userRepository;
        $this->hashService = $hashService;
    }

    /**
     * @param string $email
     * @param string $password
     * @return User|null
     */
    public function attempt(string $email, string $password): ?User
    {
        $user = $this->userRepository->findByEmail($email);

        if (is_null($user) || !$this->hashService->check($password, $user->password)) {
            return null;
        }

        return $user;
    }

    /**
     * @param User $user
     * @param array|string[] $abilities
     * @return NewAccessToken
     */
    public function createToken(User $user, array $abilities = ['*']): NewAccessToken
    {
        return $user->createToken(config('app.name'), $abilities);
    }
}
