<?php

namespace App\Contracts\Repositories;

use App\Models\Category;
use Illuminate\Database\Eloquent\Collection;

interface CategoryRepositoryInterface
{
    /**
     * @return Collection
     */
    public function getAll(): Collection;

    /**
     * @param array $attributes
     * @return Category
     */
    public function store(array $attributes): Category;

    /**
     * @param Category $category
     * @param array $attributes
     * @return bool
     */
    public function update(Category $category, array $attributes): bool;

    /**
     * @param Category $category
     * @return bool
     */
    public function destroy(Category $category): bool;
}
