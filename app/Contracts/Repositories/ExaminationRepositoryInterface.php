<?php

namespace App\Contracts\Repositories;

use App\Models\Examination;
use Illuminate\Database\Eloquent\Collection;

interface ExaminationRepositoryInterface
{
    /**
     * @return Collection
     */
    public function getAll(): Collection;

    /**
     * @param array $attributes
     * @return Examination
     */
    public function store(array $attributes): Examination;

    /**
     * @param Examination $examination
     * @param array $attributes
     * @return bool
     */
    public function update(Examination $examination, array $attributes): bool;

    /**
     * @param Examination $examination
     * @return bool
     */
    public function destroy(Examination $examination): bool;

    /**
     * @param Examination $examination
     * @param array $categoryUuids
     * @return array
     */
    public function syncCategories(Examination $examination, array $categoryUuids): array;
}
