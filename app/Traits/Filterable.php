<?php

namespace App\Traits;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Pipeline\Pipeline;

trait Filterable
{
    /**
     * @param Builder $query
     * @return Builder
     */
    public static function scopeWithFilters(Builder $query): Builder
    {
        return app(Pipeline::class)
            ->send($query)
            ->through(self::queryFilters())
            ->thenReturn();
    }

    /**
     * @return array
     */
    abstract protected static function queryFilters(): array;
}
