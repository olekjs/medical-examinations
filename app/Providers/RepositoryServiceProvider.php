<?php

namespace App\Providers;

use App\Contracts\Repositories\CategoryRepositoryInterface;
use App\Contracts\Repositories\ExaminationRepositoryInterface;
use App\Contracts\Repositories\UserRepositoryInterface;
use App\Decorators\CategoryRepositoryDecorator;
use App\Decorators\ExaminationRepositoryDecorator;
use App\Repositories\UserRepository;
use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register(): void
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot(): void
    {
        $this->app->bind(UserRepositoryInterface::class, UserRepository::class);
        $this->app->bind(CategoryRepositoryInterface::class, CategoryRepositoryDecorator::class);
        $this->app->bind(ExaminationRepositoryInterface::class, ExaminationRepositoryDecorator::class);
    }
}
