<?php

namespace App\Models;

use App\Pivots\CategoryExamination;
use App\Traits\Filterable;
use App\Traits\HasUuid;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use App\Models\Category;
use App\QueryFilters\Examination\Category as CategoryQueryFilter;
use App\QueryFilters\Examination\Search as SearchQueryFilter;

class Examination extends Model
{
    use HasFactory, HasUuid, Filterable;

    /**
     * @var string[]
     */
    protected $fillable = [
        'name',
        'icd_10_code',
        'short_description',
        'preparation_instructions',
    ];

    /**
     * @return string[]
     */
    public static function queryFilters(): array
    {
        return [
            CategoryQueryFilter::class,
            SearchQueryFilter::class,
        ];
    }

    /**
     * @return BelongsToMany
     */
    public function categories(): BelongsToMany
    {
        return $this->belongsToMany(Category::class)->using(CategoryExamination::class);
    }
}
