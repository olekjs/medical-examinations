<?php

namespace App\Models;

use App\Pivots\CategoryExamination;
use App\Traits\HasUuid;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use App\Models\Examination;

class Category extends Model
{
    use HasFactory, HasUuid;

    /**
     * @var string[]
     */
    protected $fillable = [
        'name'
    ];

    /**
     * @return BelongsToMany
     */
    public function examinations(): BelongsToMany
    {
        return $this->belongsToMany(Examination::class)->using(CategoryExamination::class);
    }
}
