<?php

namespace App\Console\Commands;

use App\Models\User;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Str;

class BuildApp extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'medical:examinations';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Build application';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $email = Str::random(3) . '@medical.app';
        $password = Str::random(5);

        $actions = collect([
            [
                'name' => 'Czyszczenie pamięci podręczniej',
                'sleep' => 1,
                'call' => function () {
                    Artisan::call('cache:clear');
                },
            ],
            [
                'name' => 'Migracja bazy danych',
                'sleep' => 1,
                'call' => function () {
                    Artisan::call('migrate:fresh --force');
                },
            ],
            [
                'name' => 'Tworzenie kategorii i badań',
                'sleep' => 2,
                'call' => function () {
                    Artisan::call('db:seed --force');
                },
            ],
            [
                'name' => 'Tworzenie użytkownika',
                'sleep' => 2,
                'call' => function () use ($email, $password) {
                    User::create([
                        'email' => $email,
                        'password' => bcrypt($password),
                    ]);
                }
            ],
        ]);

        $bar = $this->output->createProgressBar($actions->count());

        $this->line($actions->first()['name']);

        $bar->start();

        $actions->each(function ($action, $index) use ($bar) {
            if ($index > 0) {
                $this->line($action['name']);
            }

            $action['call']();

            sleep($action['sleep']);
            $bar->advance();
            $this->newLine(2);
        });

        $bar->finish();

        $this->info('Utworzono użytkownika!');
        $this->line('Email: ' . $email);
        $this->line('Hasło: ' . $password);

        return 0;
    }
}
