<?php

namespace App\QueryFilters\Examination;

use App\QueryFilters\QueryFilter;
use Illuminate\Database\Eloquent\Builder;

class Category extends QueryFilter
{
    /**
     * @param Builder $builder
     * @return Builder
     */
    protected function applyFilter(Builder $builder): Builder
    {
        return $builder->whereRelation('categories', 'id', $this->filterValue());
    }
}
