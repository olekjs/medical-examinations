<?php

namespace App\QueryFilters\Examination;

use App\QueryFilters\QueryFilter;
use Illuminate\Database\Eloquent\Builder;

class Search extends QueryFilter
{
    /**
     * @param Builder $builder
     * @return Builder
     */
    protected function applyFilter(Builder $builder): Builder
    {
        $keywords = explode(' ', $this->filterValue());

        collect($keywords)->each(function ($keyword) use ($builder) {
            $builder
                ->where('name', 'like', "%$keyword%")
                ->orWhere('icd_10_code', 'like', "%$keyword%")
                ->orWhere('short_description', 'like', "%$keyword%")
                ->orWhere('preparation_instructions', 'like', "%$keyword%");
        });

        return $builder;
    }
}
