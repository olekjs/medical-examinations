<?php

namespace App\QueryFilters;

use Closure;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Str;

abstract class QueryFilter
{
    /**
     * @param $request
     * @param Closure $next
     * @return Builder
     */
    public function handle($request, Closure $next): Builder
    {
        if (!request()->has('filters') || !request()->filled('filters.' . $this->filterName())) {
            return $next($request);
        }

        $builder = $next($request);

        return $this->applyFilter($builder);
    }

    /**
     * @return string
     */
    protected function filterName(): string
    {
        return Str::snake(class_basename($this));
    }

    /**
     * @return mixed
     */
    protected function filterValue()
    {
        return request()->input('filters.' . $this->filterName());
    }

    /**
     * @param Builder $builder
     * @return Builder
     */
    abstract protected function applyFilter(Builder $builder): Builder;
}
