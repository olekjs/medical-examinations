<?php

namespace App\Decorators;

use App\Contracts\Repositories\ExaminationRepositoryInterface;
use App\Models\Examination;
use App\Repositories\ExaminationRepository;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Contracts\Cache\Factory as CacheService;

class ExaminationRepositoryDecorator implements ExaminationRepositoryInterface
{
    /**
     * @var ExaminationRepository
     */
    private ExaminationRepository $examinationRepository;

    /**
     * @var CacheService
     */
    private CacheService $cacheService;

    /**
     * ExaminationRepositoryDecorator constructor.
     * @param ExaminationRepository $examinationRepository
     * @param CacheService $cacheService
     */
    public function __construct(
        ExaminationRepository $examinationRepository,
        CacheService $cacheService
    )
    {
        $this->examinationRepository = $examinationRepository;
        $this->cacheService = $cacheService;
    }

    /**
     * @return Collection
     */
    public function getAll(): Collection
    {
        return $this->cacheService->remember('examinations', now()->addHours(12), function () {
            return $this->examinationRepository->getAll();
        });
    }

    /**
     * @param array $attributes
     * @return Examination
     */
    public function store(array $attributes): Examination
    {
        return $this->examinationRepository->store($attributes);
    }

    /**
     * @param Examination $examination
     * @param array $attributes
     * @return bool
     */
    public function update(Examination $examination, array $attributes): bool
    {
        return $this->examinationRepository->update($examination, $attributes);
    }

    /**
     * @param Examination $examination
     * @return bool
     */
    public function destroy(Examination $examination): bool
    {
        return $this->examinationRepository->destroy($examination);
    }

    /**
     * @param Examination $examination
     * @param array $categoryUuids
     * @return array
     */
    public function syncCategories(Examination $examination, array $categoryUuids): array
    {
        return $this->examinationRepository->syncCategories($examination, $categoryUuids);
    }
}
