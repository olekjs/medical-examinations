<?php

namespace App\Decorators;

use App\Contracts\Repositories\CategoryRepositoryInterface;
use App\Models\Category;
use App\Repositories\CategoryRepository;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Contracts\Cache\Factory as CacheService;

class CategoryRepositoryDecorator implements CategoryRepositoryInterface
{
    /**
     * @var CategoryRepository
     */
    private CategoryRepository $categoryRepository;

    /**
     * @var CacheService
     */
    private CacheService $cacheService;

    /**
     * CategoryRepositoryDecorator constructor.
     * @param CategoryRepository $categoryRepository
     * @param CacheService $cacheService
     */
    public function __construct(
        CategoryRepository $categoryRepository,
        CacheService $cacheService
    )
    {
        $this->categoryRepository = $categoryRepository;
        $this->cacheService = $cacheService;
    }

    /**
     * @return Collection
     */
    public function getAll(): Collection
    {
        return $this->cacheService->remember('categories', now()->addHours(12), function () {
            return $this->categoryRepository->getAll();
        });
    }

    /**
     * @param array $attributes
     * @return Category
     */
    public function store(array $attributes): Category
    {
        return $this->categoryRepository->store($attributes);
    }

    /**
     * @param Category $category
     * @param array $attributes
     * @return bool
     */
    public function update(Category $category, array $attributes): bool
    {
        return $this->categoryRepository->update($category, $attributes);
    }

    /**
     * @param Category $category
     * @return bool
     */
    public function destroy(Category $category): bool
    {
        return $this->categoryRepository->destroy($category);
    }
}
