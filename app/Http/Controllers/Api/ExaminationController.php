<?php

namespace App\Http\Controllers\Api;

use App\Contracts\Repositories\ExaminationRepositoryInterface;
use App\Http\Controllers\Controller;
use App\Models\Examination;
use Illuminate\Http\JsonResponse;

class ExaminationController extends Controller
{
    /**
     * @var ExaminationRepositoryInterface
     */
    private ExaminationRepositoryInterface $examinationRepository;

    /**
     * CategoryController constructor.
     * @param ExaminationRepositoryInterface $examinationRepository
     */
    public function __construct(ExaminationRepositoryInterface $examinationRepository)
    {
        $this->examinationRepository = $examinationRepository;
    }

    /**
     * @return JsonResponse
     */
    public function index(): JsonResponse
    {
        return response()->json(
            $this->examinationRepository->getAll(),
        );
    }

    /**
     * @param Examination $examination
     * @return JsonResponse
     */
    public function show(Examination $examination): JsonResponse
    {
        return response()->json(
            $examination->load('categories')
        );
    }
}
