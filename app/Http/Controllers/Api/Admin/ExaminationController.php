<?php

namespace App\Http\Controllers\Api\Admin;

use App\Contracts\Repositories\ExaminationRepositoryInterface;
use App\Http\Controllers\Controller;
use App\Http\Requests\Api\Admin\Examination\StoreRequest;
use App\Http\Requests\Api\Admin\Examination\SyncCategoriesRequest;
use App\Http\Requests\Api\Admin\Examination\UpdateRequest;
use App\Models\Examination;
use Illuminate\Http\JsonResponse;

class ExaminationController extends Controller
{
    /**
     * @var ExaminationRepositoryInterface
     */
    private ExaminationRepositoryInterface $examinationRepository;

    /**
     * ExaminationController constructor.
     * @param ExaminationRepositoryInterface $examinationRepository
     */
    public function __construct(ExaminationRepositoryInterface $examinationRepository)
    {
        $this->examinationRepository = $examinationRepository;
    }

    /**
     * @param StoreRequest $request
     * @return JsonResponse
     */
    public function store(StoreRequest $request): JsonResponse
    {
        return response()->json(
            $this->examinationRepository->store($request->validated()),
            201
        );
    }

    /**
     * @param UpdateRequest $request
     * @param Examination $examination
     * @return JsonResponse
     */
    public function update(UpdateRequest $request, Examination $examination): JsonResponse
    {
        return response()->json(
            $this->examinationRepository->update(
                $examination,
                $request->validated()
            ),
        );
    }

    /**
     * @param Examination $examination
     * @return JsonResponse
     */
    public function destroy(Examination $examination): JsonResponse
    {
        return response()->json(
            $this->examinationRepository->destroy(
                $examination,
            ),
        );
    }

    public function syncCategories(SyncCategoriesRequest $request, Examination $examination): JsonResponse
    {
        return response()->json(
            $this->examinationRepository->syncCategories(
                $examination,
                $request->input('categories')
            ),
        );
    }
}
