<?php

namespace App\Http\Controllers\Api;

use App\Contracts\Repositories\UserRepositoryInterface;
use App\Http\Controllers\Controller;
use App\Http\Requests\Api\Auth\LoginRequest;
use App\Services\AuthService;
use Illuminate\Http\JsonResponse;

class AuthController extends Controller
{
    /**
     * @var AuthService
     */
    private AuthService $authService;

    /**
     * AuthController constructor.
     * @param AuthService $authService
     */
    public function __construct(
        AuthService $authService
    )
    {
        $this->authService = $authService;
    }

    /**
     * @param LoginRequest $request
     * @return JsonResponse
     */
    public function login(LoginRequest $request): JsonResponse
    {
        $authUser = $this->authService->attempt(
            $request->input('email'),
            $request->input('password'),
        );

        if (is_null($authUser)) {
            return response()->json([
                'message' => 'Invalid credentials'
            ], 401);
        }

        return response()->json(
            $this->authService->createToken($authUser)->plainTextToken
        );
    }
}
