<?php

namespace App\Http\Requests\Api\Admin\Examination;

use Illuminate\Foundation\Http\FormRequest;

class StoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'name' => 'required',
            'icd_10_code'=> 'required',
            'short_description'=> 'nullable',
            'preparation_instructions'=> 'nullable',
        ];
    }
}
