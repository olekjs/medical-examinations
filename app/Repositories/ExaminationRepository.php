<?php

namespace App\Repositories;

use App\Contracts\Repositories\ExaminationRepositoryInterface;
use App\Models\Examination;
use Illuminate\Database\Eloquent\Collection;

class ExaminationRepository implements ExaminationRepositoryInterface
{
    /**
     * @return Collection
     */
    public function getAll(): Collection
    {
        return Examination::query()
            ->with('categories')
            ->withFilters()
            ->latest()
            ->get();
    }

    /**
     * @param array $attributes
     * @return Examination
     */
    public function store(array $attributes): Examination
    {
        return Examination::create($attributes);
    }

    /**
     * @param Examination $examination
     * @param array $attributes
     * @return bool
     */
    public function update(Examination $examination, array $attributes): bool
    {
        return $examination->update($attributes);
    }

    /**
     * @param Examination $examination
     * @return bool
     */
    public function destroy(Examination $examination): bool
    {
        return $examination->delete();
    }

    /**
     * @param Examination $examination
     * @param array $categoryUuids
     * @return array
     */
    public function syncCategories(Examination $examination, array $categoryUuids): array
    {
        return $examination->categories()->sync($categoryUuids);
    }
}
