<?php

namespace App\Repositories;

use App\Contracts\Repositories\CategoryRepositoryInterface;
use App\Models\Category;
use Illuminate\Database\Eloquent\Collection;

class CategoryRepository implements CategoryRepositoryInterface
{
    /**
     * @return Collection
     */
    public function getAll(): Collection
    {
        return Category::latest()->get();
    }

    /**
     * @param array $attributes
     * @return Category
     */
    public function store(array $attributes): Category
    {
        return Category::create($attributes);
    }

    /**
     * @param Category $category
     * @param array $attributes
     * @return bool
     */
    public function update(Category $category, array $attributes): bool
    {
        return $category->update($attributes);
    }

    /**
     * @param Category $category
     * @return bool
     */
    public function destroy(Category $category): bool
    {
        return $category->delete();
    }
}
