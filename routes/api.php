<?php

use App\Http\Controllers\Api\AuthController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Api\CategoryController;
use App\Http\Controllers\Api\ExaminationController;

Route::post('auth/login', [AuthController::class, 'login'])->name('login');

Route::apiResource('categories', CategoryController::class)->only([
    'index',
    'show',
]);

Route::apiResource('examinations', ExaminationController::class)->only([
    'index',
    'show',
]);
