<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Api\Admin\CategoryController;
use App\Http\Controllers\Api\Admin\ExaminationController;

Route::apiResource('categories', CategoryController::class)->only([
    'store',
    'update',
    'destroy'
]);

Route::prefix('examinations')->name('examinations.')->group(function () {
    Route::post(
        '{examination}/sync-categories',
        [ExaminationController::class, 'syncCategories']
    )->name('sync.categories');
});

Route::apiResource('examinations', ExaminationController::class)->only([
    'store',
    'update',
    'destroy'
]);
